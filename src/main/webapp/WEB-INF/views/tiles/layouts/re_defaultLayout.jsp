<%-- 
    Document   : re_defaultLayout
    Created on : May 14, 2017, 5:39:14 PM
    Author     : Sergei
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="utf-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><tiles:getAsString name="title" /></title>
        <link href="<c:url value='/static/css/templatemo_style.css' />"  rel="stylesheet"></link>
        <link href="<c:url value='/static/css/gallery_style.css' />" rel="stylesheet"></link>
        
</head>

<body>
    <div id="templatemo_container">
        <tiles:insertAttribute name="re_top_panel" />
        
        <tiles:insertAttribute name="re_templatemo_gallery_panel" />
        
        
        <tiles:insertAttribute name="re_templatemo_content_panel_1" />
        
        <tiles:insertAttribute name="re_templatemo_content_panel_2" />
        
        <tiles:insertAttribute name="re_templatemo_content_panel_3" />
        
        <tiles:insertAttribute name="re_templatemo_footer_panel" />
        
        
    </div>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/slide.js"></script>

</body>
</html>
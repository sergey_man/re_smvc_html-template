<%-- 
    Document   : top_panel
    Created on : May 14, 2017, 5:46:38 PM
    Author     : Sergei
--%>

<div id="templatemo_top_panel">
    <div id="templatemo_header_section">
        <div id="templatemo_header">
            Real Estate
        </div>
    </div> <!-- end of header section -->

    <div id="templatemo_menu_login_section">
        <div id="templatemo_menu_section">
            <div id="templatemo_menu_panel">
                <ul>
                    <li><a href="index.html" class="current">Home</a></li>
                    <li><a href="subpage.html">Buy</a></li>
                    <li><a href="#">Rent</a></li>
                    <li><a href="#">Mortgage</a></li>
                    <li><a href="#">About</a></li>
                    <li><a href="#">Contact</a></li>                   
                </ul> 
            </div> <!-- end of menu -->
        </div>
        <div id="templatemo_login_section">
            <form method="get" action="#">
                <label>Email Address:</label><input type="text" name="email" class="input_field" /><label>Password:</label><input type="password" name="password" class="input_field" /><input type="submit" value="" name="submit" class="submit_btn" />
            </form>
        </div>
    </div> <!-- end of menu login section -->
</div> <!-- end of top panel -->

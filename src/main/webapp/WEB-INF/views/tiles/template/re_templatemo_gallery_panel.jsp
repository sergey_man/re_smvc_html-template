<%-- 
    Document   : re_templato_gallary_panel
    Created on : May 14, 2017, 5:50:11 PM
    Author     : Sergei
--%>

    
<div id="templatemo_gallery_panel">
    <div id="gallery">
        <div id="imagearea">
            <div id="image">
                <a href="javascript:slideShow.nav(-1)" class="imgnav " id="previmg"></a>
                <a href="javascript:slideShow.nav(1)" class="imgnav " id="nextimg"></a>
            </div>
        </div>
        <div id="thumbwrapper">
            <div id="thumbarea">
                <ul id="thumbs">
                    <li value="1"><img src="${pageContext.request.contextPath}/static/img/thumbs/1.jpg" width="179" height="100" alt="" /></li>
                    <li value="2"><img src="${pageContext.request.contextPath}/static/img/thumbs/2.jpg" width="179" height="100" alt="" /></li>
                    <li value="3"><img src="${pageContext.request.contextPath}/static/img/thumbs/3.jpg" width="179" height="100" alt="" /></li>
                    <li value="4"><img src="${pageContext.request.contextPath}/static/img/thumbs/4.jpg" width="179" height="100" alt="" /></li>
                    <li value="5"><img src="${pageContext.request.contextPath}/static/img/thumbs/5.jpg" width="179" height="100" alt="" /></li>
                    <li value="3"><img src="${pageContext.request.contextPath}/static/img/thumbs/3.jpg" width="179" height="100" alt="" /></li>
                    <li value="4"><img src="${pageContext.request.contextPath}/static/img/thumbs/4.jpg" width="179" height="100" alt="" /></li>
                    <li value="5"><img src="${pageContext.request.contextPath}/static/img/thumbs/5.jpg" width="179" height="100" alt="" /></li>
                </ul>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var imgid = 'image';
        var imgdir = 'static/img/fullsize';
        var imgext = '.jpg';
        var thumbid = 'thumbs';
        var auto = true;
        var autodelay = 5;
        
        //alert('++' + ${pageContext.request.contextPath} + '++');
        
        
    </script>
<!--    <script type="text/javascript" src="../../../../static/js/slide.js"></script>-->
<!--<script type="text/javascript" src="/static/js/slide.js"></script>-->
<!--<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/slide.js"></script>-->

</div>
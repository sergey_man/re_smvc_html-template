<%-- 
    Document   : re_templatemo_content_panel_3
    Created on : May 14, 2017, 5:53:23 PM
    Author     : Sergei
--%>


<div id="templatemo_content_panel_3">

    <div class="templatemo_quick_contact">
        <h1>Quick Contact</h1>
        <p>
            Tel: 010-100-1000<br />
            Fax: 020-200-2000<br />
            Email: info {at} templatemo.com
        </p>
        <a href="http://validator.w3.org/check?uri=referer"><img style="border:0;width:88px;height:31px" src="http://www.w3.org/Icons/valid-xhtml10" alt="Valid XHTML 1.0 Transitional" width="88" height="31" vspace="8" border="0" /></a>
        <a href="http://jigsaw.w3.org/css-validator/check/referer"><img style="border:0;width:88px;height:31px"  src="http://jigsaw.w3.org/css-validator/images/vcss-blue" alt="Valid CSS!" vspace="8" border="0" /></a> 
    </div><!-- end of news section 3-->

    <div class="templatemo_section_3">
        <h1>Helpful Links</h1>
        <ul class="list_section">
            <li><a href="#">Testimonials</a></li>
            <li><a href="#">FAQs</a></li>
            <li><a href="#">Terms of us</a></li>
            <li><a href="#">About Us</a></li>
            <li><a href="#">Contact Us</a></li>
        </ul>
    </div>
    <div class="templatemo_section_3">
        <h1>Partner Links</h1>
        <ul class="list_section">
            <li><a href="http://www.flashmo.com" target="_blank">Free Flash</a></li>
            <li><a href="http://www.templatemo.com" target="_blank">Free CSS</a></li>
            <li><a href="http://www.webdesignmo.com" target="_blank">Web Design</a></li>
            <li><a href="http://www.photovaco.com" target="_blank">Free Photos</a></li>
        </ul>
    </div>

    <div class="cleaner_with_height">&nbsp;</div>
</div><!-- end of content panel 3 -->
<%-- 
    Document   : re_templatemo_section_3
    Created on : May 14, 2017, 5:54:18 PM
    Author     : Sergei
--%>

<div class="templatemo_section_3">
    <h1>Partner Links</h1>
    <ul class="list_section">
        <li><a href="http://www.flashmo.com" target="_blank">Free Flash</a></li>
        <li><a href="http://www.templatemo.com" target="_blank">Free CSS</a></li>
        <li><a href="http://www.webdesignmo.com" target="_blank">Web Design</a></li>
        <li><a href="http://www.photovaco.com" target="_blank">Free Photos</a></li>
    </ul>
</div>

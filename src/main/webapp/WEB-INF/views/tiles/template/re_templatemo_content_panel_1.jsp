<%-- 
    Document   : re_templatemo_content_panel_1
    Created on : May 14, 2017, 5:51:44 PM
    Author     : Sergei
--%>
<div id="templatemo_content_panel_1">

    <div id="templatemo_news_section">
        <h1>Site's News</h1>
        <div class="templatemo_news_box">
            <h3>Free CSS Template</h3>
            <p>This <a href="http://www.templatemo.com" target="_parent">CSS template</a> is provided by <a href="http://www.templatemo.com/page/1" target="_parent">templatemo.com </a>for free. You may download, modify and apply this CSS template layout for your websites. Credit goes to <a href="http://publicdomainpictures.net/" target="_blank">PublicDomainPictures.net</a> for photos. <a href="#">more</a></p>
        </div>
    </div><!-- end of news section -->

    <div id="templatemo_section_1_1">
        <h1>Find a property</h1>
        <form method="get" action="#">
            <div class="left_column">
                <div class="form_row"> <label>City: </label><input type="text" name="country" value="" /> </div>
                <div class="form_row"> <label>Sate / Province: </label>
                    <select name="state"><option>Select one...</option><option>AL</option><option>AK</option><option>AR</option><option>AZ</option><option>CA</option><option>CO</option><option>CT</option><option>DC</option><option>DE</option><option>FL</option><option>GA</option><option>HI</option><option>ID</option><option>IA</option><option>IL</option><option>IN</option><option>KS</option><option>KY</option><option>LA</option><option>MA</option><option>MD</option><option>ME</option><option>MI</option><option>MO</option><option>MN</option><option>MS</option><option>MT</option><option>NC</option><option>ND</option><option>NE</option><option>NH</option><option>NJ</option><option>NM</option><option>NY</option><option>NV</option><option>OH</option><option>OK</option><option>OR</option><option>PA</option><option>RI</option><option>SC</option><option>SD</option><option>TN</option><option>TX</option><option>UT</option><option>VA</option><option>VT</option><option>WA</option><option>WI</option><option>WV</option><option>WY</option></select>
                </div>
                <div class="form_row"> <label>Zip / Postal code </label><input type="text" name="country" value="" /> </div>
            </div>
            <div class="right_column">
                <div class="form_row">
                    <label>Price: </label>
                    <select name="min_price">
                        <option>minimum</option>
                        <option>$50,000</option>
                        <option>$100,000</option>
                        <option>$150,000</option>
                        <option>$200,000</option>
                        <option>$250,000</option>
                    </select>
                    <label>To: </label>
                    <select name="max-price">
                        <option>maximum</option>
                        <option>$100,000</option>
                        <option>$200,000</option>
                        <option>$400,000</option>
                        <option>$600,000</option>
                        <option>$800,000</option>
                    </select>
                </div>
                <div class="form_row"> <label>Bedroom: </label>
                    <select name="bedroom">
                        <option>minimum</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                </div>
                <div class="form_row"> <label>Bathroom: </label>
                    <select name="bathroom">
                        <option>minimum</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                </div>
                <div class="form_row"> 
                    <input type="submit" value="Submit" name="submit" class="submit_btn_02" />
                </div>
            </div>

        </form>
    </div><!-- end of section 1 -->
    <div class="cleaner_with_height">&nbsp;</div>
</div>
